package com.company;

public class Main {

    public static void main(String[] args) {


        printEqual(1,2,3);
    }
    public static void printEqual (int firstNum, int secNum, int thirdNum) {
        if (firstNum < 0 || secNum < 0 || thirdNum < 0) {
            System.out.println("Invalid Value");
        }else if (firstNum == secNum && secNum == thirdNum) {
            System.out.println("All numbers are equal");
        } else if (firstNum != secNum && secNum != thirdNum && firstNum != thirdNum) {
            System.out.println("All numbers are different");
        }else{
            System.out.println("Neither all are equal or different");
        }

    }
}
